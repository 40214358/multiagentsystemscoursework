/**
 * 
 */
package coursework_ontology.elements;

import java.util.List;

import jade.content.onto.annotations.AggregateSlot;
import jade.content.onto.annotations.Slot;

public class Phone extends Component {
	private Storage storage; 
	private RAM ram;
	
	@Slot(mandatory = true)
	public Storage getStorage() {
		return storage;
	}
	
	public void setStorage(Storage storage) {
		this.storage = storage;
	}
	
	@Slot(mandatory = true)
	public RAM getRam() {
		return ram;
	}
	
	public void setRam(RAM ram) {
		this.ram = ram;
	}
	
	
}
