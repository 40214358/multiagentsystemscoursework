package coursework_ontology.elements;

import jade.content.onto.annotations.Slot;

public class Phablet extends Phone {
	private Battery3000 battery;
	private Screen7 screen;
	
	@Slot(mandatory = true)
	public Battery3000 getBattery() {
		return battery;
	}
	public void setBattery(Battery3000 battery) {
		this.battery = battery;
	}
	@Slot(mandatory = true)
	public Screen7 getScreen() {
		return screen;
	}
	public void setScreen(Screen7 screen) {
		this.screen = screen;
	}
}
