/**
 * 
 */
package coursework_ontology.elements;


import jade.content.Predicate;
import jade.core.AID;

public class Owns implements Predicate {
	private AID owner;
	private Component item;
	
	public AID getOwner() {
		return owner;
	}
	
	public void setOwner(AID owner) {
		this.owner = owner;
	}
	
	public Component getItem() {
		return item;
	}
	
	public void setItem(Component item) {
		this.item = item;
	}
	
}
