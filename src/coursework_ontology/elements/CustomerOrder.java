package coursework_ontology.elements;

import jade.content.AgentAction;
import jade.core.AID;

public class CustomerOrder implements AgentAction {
	private AID customer;
	private Component phone;
	//price is the unit price of one smartphone
	private int price;
	private int penalty;
	private int quantity;
	//due date will correspond to a day. ie, Day 22
	private int dueDate;
	
	
	public AID getCustomer() {
		return customer;
	}
	public void setCustomer(AID customer) {
		this.customer = customer;
	}
	public Component getPhone() {
		return phone;
	}
	public void setPhone(Component phone) {
		this.phone = phone;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public int getPenalty() {
		return penalty;
	}
	public void setPenalty(int penalty) {
		this.penalty = penalty;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public int getDueDate() {
		return dueDate;
	}
	public void setDueDate(int dueDate) {
		this.dueDate = dueDate;
	}
}
