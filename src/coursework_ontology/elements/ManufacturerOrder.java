package coursework_ontology.elements;

import jade.content.Concept;
import jade.core.AID;

public class ManufacturerOrder {
	private AID supplier;
	private int price;
	private Concept component;
	private int quantity;
	private int dueDate;
	
	public AID getSupplier() {
		return supplier;
	}
	public void setSupplier(AID supplier) {
		this.supplier = supplier;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public Concept getComponent() {
		return component;
	}
	public void setComponent(Concept component) {
		this.component = component;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public int getDueDate() {
		return dueDate;
	}
	public void setDueDate(int dueDate) {
		this.dueDate = dueDate;
	}
	
}
