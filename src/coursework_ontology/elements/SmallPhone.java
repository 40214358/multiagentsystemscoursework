/**
 * 
 */
package coursework_ontology.elements;

import jade.content.onto.annotations.Slot;

public class SmallPhone extends Phone {
	private Battery2000 battery;
	private Screen5 screen;
	
	@Slot(mandatory = true)
	public Battery2000 getBattery() {
		return battery;
	}
	public void setBattery(Battery2000 battery) {
		this.battery = battery;
	}
	@Slot(mandatory = true)
	public Screen5 getScreen() {
		return screen;
	}
	public void setScreen(Screen5 screen) {
		this.screen = screen;
	}
	
}
