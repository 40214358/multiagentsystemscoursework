package coursework;

import jade.content.Concept;
import jade.content.ContentElement;
import jade.content.lang.Codec;
import jade.content.lang.Codec.CodecException;
import jade.content.lang.sl.SLCodec;
import jade.content.onto.Ontology;
import jade.content.onto.OntologyException;
import jade.content.onto.basic.Action;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.SequentialBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import coursework.SupplierAgent.TickerWaiter;
import coursework_ontology.ECommerceOntology;
import coursework_ontology.elements.CustomerOrder;
import coursework_ontology.elements.ManufacturerOrder;
import coursework_ontology.elements.Owns;

public class ManufacturerAgent extends Agent {
	private Codec codec = new SLCodec();
	private Ontology ontology = ECommerceOntology.getInstance();

	private AID tickerAgent;
	private ArrayList<AID> suppliers = new ArrayList<>();
	private ArrayList<AID> customers = new ArrayList<>();

	private int customerOrdersProcessed = 0;

	// Stores the phones ordered by each customer and order details
	private ArrayList<CustomerOrder> customerOrders = new ArrayList<>();

	// Stores the components requested from each supplier
	private ArrayList<ManufacturerOrder> manufacturerOrders = new ArrayList<>();

	protected void setup() {
		getContentManager().registerLanguage(codec);
		getContentManager().registerOntology(ontology);

		// add this agent to the yellow pages
		DFAgentDescription dfd = new DFAgentDescription();
		dfd.setName(getAID());
		ServiceDescription sd = new ServiceDescription();
		sd.setType("manufacturer");
		sd.setName(getLocalName() + "-manufacturer-agent");
		dfd.addServices(sd);
		try {
			DFService.register(this, dfd);
		} catch (FIPAException e) {
			e.printStackTrace();
		}

		addBehaviour(new TickerWaiter(this));
	}

	public class TickerWaiter extends CyclicBehaviour {

		// behaviour to wait for a new day
		public TickerWaiter(Agent a) {
			super(a);
		}

		@Override
		public void action() {
			MessageTemplate mt = MessageTemplate.or(
					MessageTemplate.MatchContent("new day"),
					MessageTemplate.MatchContent("terminate"));
			ACLMessage msg = myAgent.receive(mt);
			if (msg != null) {
				if (tickerAgent == null) {
					tickerAgent = msg.getSender();
				}
				if (msg.getContent().equals("new day")) {
					// spawn new sequential behaviour for day's activities
					SequentialBehaviour dailyActivity = new SequentialBehaviour();
					// sub-behaviours will execute in the order they are added
					dailyActivity.addSubBehaviour(new FindSuppliers(myAgent));
					dailyActivity.addSubBehaviour(new FindCustomers(myAgent));
					dailyActivity.addSubBehaviour(new ProcessCustomerOrders(myAgent));
					dailyActivity.addSubBehaviour(new EndDay(myAgent));
					myAgent.addBehaviour(dailyActivity);
				} else {
					// termination message to end simulation
					myAgent.doDelete();
				}
			} else {
				block();
			}
		}

	}

	public class FindSuppliers extends OneShotBehaviour {

		public FindSuppliers(Agent a) {
			super(a);
		}

		@Override
		public void action() {
			DFAgentDescription supplierTemplate = new DFAgentDescription();
			ServiceDescription sd = new ServiceDescription();
			sd.setType("supplier");
			supplierTemplate.addServices(sd);
			try {
				suppliers.clear();
				DFAgentDescription[] agentsType1 = DFService.search(myAgent,
						supplierTemplate);
				for (int i = 0; i < agentsType1.length; i++) {
					suppliers.add(agentsType1[i].getName()); // this is the AID
				}
				System.out.println("Manufacturer found " + suppliers.size()
						+ " Suppliers");
			} catch (FIPAException e) {
				e.printStackTrace();
			}

		}

	}

	public class FindCustomers extends OneShotBehaviour {

		public FindCustomers(Agent a) {
			super(a);
		}

		@Override
		public void action() {
			DFAgentDescription customerTemplate = new DFAgentDescription();
			ServiceDescription sd = new ServiceDescription();
			sd.setType("customer");
			customerTemplate.addServices(sd);
			try {
				customers.clear();
				DFAgentDescription[] agentsType1 = DFService.search(myAgent,
						customerTemplate);
				for (int i = 0; i < agentsType1.length; i++) {
					customers.add(agentsType1[i].getName()); // this is the AID
				}
				System.out.println("Manufacturer found " + suppliers.size()
						+ " Customers");
			} catch (FIPAException e) {
				e.printStackTrace();
			}

		}

	}

	private class ProcessCustomerOrders extends Behaviour {
		public ProcessCustomerOrders(Agent a) {
			super(a);
		}
		
		@Override
		public void onStart(){
			customerOrdersProcessed = 0;
		}

		@Override
		public void action() {
			customerOrdersProcessed = 0;
			// This behaviour should only respond to QUERY_IF messages
			MessageTemplate mt = MessageTemplate
					.MatchPerformative(ACLMessage.REQUEST);
			ACLMessage msg = receive(mt);
			if (msg != null) {
				try {
					ContentElement ce = null;
					System.out.println(msg.getContent()); // print out the
															// message content
															// in SL

					// Let JADE convert from String to Java objects
					// Output will be a ContentElement
					ce = getContentManager().extractContent(msg);
					if (ce instanceof Action) {
						Concept action = ((Action)ce).getAction();
						// Randomly accept or decline orders
						Random random = new Random();
						if (random.nextBoolean() == true) {
							// Accept order from customer
							// Add customer order to the list
							customerOrders.add((CustomerOrder)action);
							System.out.println((CustomerOrder)action);
							customerOrdersProcessed++;

						} else {
							// Decline order from customer
//							ACLMessage buyRequest = new ACLMessage(
//									ACLMessage.REJECT_PROPOSAL);
//							buyRequest.addReceiver(msg.getSender());
							customerOrdersProcessed++;
						}

					}
				}

				catch (CodecException ce) {
					ce.printStackTrace();
				} catch (OntologyException oe) {
					oe.printStackTrace();
				}

			} else {
				block();
			}
		}

		@Override
		public boolean done() {
			return customerOrdersProcessed == customers.size();
		}
	}

	public class EndDay extends OneShotBehaviour {
		private List<Behaviour> toRemove;
		private boolean manufacturerFinished = false;

		public EndDay(Agent a) {
			super(a);
		}

		@Override
		public void action() {
			ACLMessage dayDutiesComplete = new ACLMessage(ACLMessage.INFORM);
			dayDutiesComplete.setContent("done");
			dayDutiesComplete.addReceiver(tickerAgent);
			myAgent.send(dayDutiesComplete);

			ACLMessage supplierDone = new ACLMessage(ACLMessage.INFORM);
			supplierDone.setContent("done");
			for (AID supplier : suppliers) {
				supplierDone.addReceiver(supplier);
			}
			myAgent.send(supplierDone);

			// }
		}

	}

}
