package coursework;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import coursework_ontology.ECommerceOntology;
import coursework_ontology.elements.Battery2000;
import coursework_ontology.elements.Battery3000;
import coursework_ontology.elements.CustomerOrder;
import coursework_ontology.elements.Phablet;
import coursework_ontology.elements.Phone;
import coursework_ontology.elements.RAM;
import coursework_ontology.elements.Screen5;
import coursework_ontology.elements.Screen7;
import coursework_ontology.elements.SmallPhone;
import coursework_ontology.elements.Storage;
import jade.content.lang.Codec;
import jade.content.lang.Codec.CodecException;
import jade.content.lang.sl.SLCodec;
import jade.content.onto.Ontology;
import jade.content.onto.OntologyException;
import jade.content.onto.basic.Action;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.SequentialBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.StringACLCodec;

public class CustomerAgent extends Agent {
	private Codec codec = new SLCodec();
	private Ontology ontology = ECommerceOntology.getInstance();
	
	private HashMap<String,ArrayList<CustomerOrder>> currentOffers = new HashMap<>();
	private AID tickerAgent;
	private AID manufacturerAID;
	private int numQueriesSent;
	private ArrayList<AID> manufacturers = new ArrayList<>();

	@Override
	protected void setup() {
		getContentManager().registerLanguage(codec);
		getContentManager().registerOntology(ontology);
		
		//add this agent to the yellow pages
		DFAgentDescription dfd = new DFAgentDescription();
		dfd.setName(getAID());
		ServiceDescription sd = new ServiceDescription();
		sd.setType("customer");
		sd.setName(getLocalName() + "-customer-agent");
		dfd.addServices(sd);
		try{
			DFService.register(this, dfd);
		}
		catch(FIPAException e){
			e.printStackTrace();
		}
		addBehaviour(new TickerWaiter(this));
	}


	@Override
	protected void takeDown() {
		//Deregister from the yellow pages
		try{
			DFService.deregister(this);
		}
		catch(FIPAException e){
			e.printStackTrace();
		}
	}

	public class TickerWaiter extends CyclicBehaviour {

		//behaviour to wait for a new day
		public TickerWaiter(Agent a) {
			super(a);
		}

		@Override
		public void action() {
			MessageTemplate mt = MessageTemplate.or(MessageTemplate.MatchContent("new day"),
					MessageTemplate.MatchContent("terminate"));
			ACLMessage msg = myAgent.receive(mt); 
			if(msg != null) {
				if(tickerAgent == null) {
					tickerAgent = msg.getSender();
				}
				if(msg.getContent().equals("new day")) {
					//spawn new sequential behaviour for day's activities
					SequentialBehaviour dailyActivity = new SequentialBehaviour();
					//sub-behaviours will execute in the order they are added
					dailyActivity.addSubBehaviour(new FindManufacturer(myAgent));
					dailyActivity.addSubBehaviour(new RequestOrder(myAgent));
					dailyActivity.addSubBehaviour(new EndDay(myAgent));
					myAgent.addBehaviour(dailyActivity);
				}
				else {
					//termination message to end simulation
					myAgent.doDelete();
				}
			}
			else{
				block();
			}
		}

	}

	public class FindManufacturer extends OneShotBehaviour {

		public FindManufacturer(Agent a) {
			super(a);
		}

		@Override
		public void action() {
			DFAgentDescription manufacturerTemplate = new DFAgentDescription();
			ServiceDescription sd = new ServiceDescription();
			sd.setType("manufacturer");
			manufacturerTemplate.addServices(sd);
			try {
				manufacturers.clear();
				DFAgentDescription[] agentsType1 = DFService.search(myAgent,
						manufacturerTemplate);
				for (int i = 0; i < agentsType1.length; i++) {
					manufacturers.add(agentsType1[i].getName()); // this is the AID
				}
			System.out.println("Customer found " + manufacturers.size() + " Manufacturers");
			if (manufacturers.size() > 0){
				manufacturerAID = manufacturers.get(0);
			}
			} catch (FIPAException e) {
				e.printStackTrace();
			}

		}

	}
	
	
	
	public class RequestOrder extends OneShotBehaviour{
		public RequestOrder(Agent a) {
			super(a);
		}
		
		public void action(){
			
			// Prepare the action request message
			ACLMessage msg = new ACLMessage(ACLMessage.REQUEST);
			msg.addReceiver(manufacturerAID); 
			msg.setLanguage(codec.getName());
			msg.setOntology(ontology.getName()); 
			
			double randSize = Math.random();
			double randRAM = Math.random();
			double randStorage = Math.random();
			Phone phoneSpecification = new Phone();
			
			if (randSize < 0.5) {
				//Phone is a small phone, set specifications accordingly
				phoneSpecification = new SmallPhone();
				((SmallPhone)(phoneSpecification)).setBattery(new Battery2000());
				((SmallPhone)(phoneSpecification)).setScreen(new Screen5());
			}
			else{
				//Phone is a phablet, set specifications accordingly
				phoneSpecification = new Phablet();
				((Phablet)(phoneSpecification)).setBattery(new Battery3000());
				((Phablet)(phoneSpecification)).setScreen(new Screen7());
			}
			if (randRAM < 0.5) {
				//Ram is 4GB
				RAM phoneRam = new RAM();
				phoneRam.setRam(4);
				phoneSpecification.setRam(phoneRam);
			}
			else{
				//Ram is 8GB
				RAM phoneRam = new RAM();
				phoneRam.setRam(8);
				phoneSpecification.setRam(phoneRam);
			}
			if (randStorage < 0.5) {
				//Storage is 64GB
				Storage phoneStorage = new Storage();
				phoneStorage.setStorage(64);
				phoneSpecification.setStorage(phoneStorage);
			}
			else{
				//Storage is 256GB
				Storage phoneStorage = new Storage();
				phoneStorage.setStorage(256);
				phoneSpecification.setStorage(phoneStorage);
			}
			
			//Customer Order
			CustomerOrder custOrder = new CustomerOrder();
			custOrder.setCustomer(myAgent.getAID());
			custOrder.setPhone(phoneSpecification);
			custOrder.setQuantity((int)Math.floor(1+50 * Math.random()));
			custOrder.setPrice((int)(custOrder.getQuantity() * Math.floor(1+500 * Math.random())));
			custOrder.setDueDate((int)Math.floor(1+10 * Math.random()));
			custOrder.setPenalty((int)(custOrder.getQuantity() * Math.floor(1+50 * Math.random())));
			
			
			
			//IMPORTANT: According to FIPA, we need to create a wrapper Action object 
			//with the action and the AID of the agent
			//we are requesting to perform the action
			//you will get an exception if you try to send the sell action directly
			//not inside the wrapper!!!
			Action request = new Action();
			request.setAction(custOrder);
			request.setActor(manufacturerAID); // the agent that you request to perform the action
			try {
			 // Let JADE convert from Java objects to string
			 getContentManager().fillContent(msg, request); //send the wrapper object
			 send(msg);
			}
			catch (CodecException ce) {
			 ce.printStackTrace();
			}
			catch (OntologyException oe) {
			 oe.printStackTrace();
			} 
		}
	}




	
	
//	public class ReplyOffers extends OneShotBehaviour {
//		
//		private int respondedOffers = 0;
//		
//		public ReplyOffers(Agent a) {
//			super(a);
//		}
//		
//		public void action(){
//			
//			for(HashMap.Entry<String, ArrayList<Order>> entry : currentOffers.entrySet()) {
//			    String key = entry.getKey();
//			    ArrayList<Order> Offers = entry.getValue();
//
//				Random rand = new Random();
//				boolean r = rand.nextBoolean();
//				
//				for(Order offer: Offers){
//					
//
//				//randomly accept offer 
//				if (r) {
//					MessageTemplate mt = MessageTemplate.MatchPerformative(ACLMessage.CFP);
//					ACLMessage response = new ACLMessage(ACLMessage.ACCEPT_PROPOSAL);
//					AID reciever = offer.getSeller();
//					response.addReceiver(reciever);
//					response.setContent("Reply to offer");
//					response.setConversationId(key);
//					myAgent.send(response);
//				}
//				//else, randomly decline offer
//				else {
//					MessageTemplate mt = MessageTemplate.MatchPerformative(ACLMessage.CFP);
//					ACLMessage response = new ACLMessage(ACLMessage.REJECT_PROPOSAL);
//					AID reciever = offer.getSeller();
//					response.addReceiver(reciever);
//					response.setContent("Reply to offer");
//					myAgent.send(response);
//				}
//				}
//			}
//		}
//	}
	
//	public class ResponseListener extends Behaviour {
//
//		public ResponseListener(Agent a) {
//			super(a);
//		}
//
//		// Book has been purchased by the seller
//		@Override
//		public void action() {
//			boolean purchased = false;
//			MessageTemplate mt = MessageTemplate.and(
//					MessageTemplate.MatchContent("Purchased book!"),
//					MessageTemplate.MatchPerformative(ACLMessage.INFORM));
//			ACLMessage msg = myAgent.receive(mt);
//			if (msg != null && msg.getSender() != tickerAgent) {
//				System.out.println("SOLD!!");
//				purchased = true;
//			} else {
//				if (!purchased) {
//					block();
//				}
//
//			}
//
//		}
//
//		public boolean done() {
//			return true;
//		}
//
//	}
//	
	public class EndDay extends OneShotBehaviour {
		
		public EndDay(Agent a) {
			super(a);
		}

		@Override
		public void action() {
			ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
			msg.addReceiver(tickerAgent);
			msg.setContent("done");
			myAgent.send(msg);
		}
		
	}

}





