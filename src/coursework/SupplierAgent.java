package coursework;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import coursework_ontology.ECommerceOntology;
import jade.content.lang.Codec;
import jade.content.lang.sl.SLCodec;
import jade.content.onto.Ontology;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

public class SupplierAgent extends Agent {
	private Codec codec = new SLCodec();
	private Ontology ontology = ECommerceOntology.getInstance();
	
	private HashMap<String,Integer> componentsForSale = new HashMap<>();
	private AID tickerAgent;
	private ArrayList<AID> buyers = new ArrayList<>();
	@Override
	protected void setup() {
		getContentManager().registerLanguage(codec);
		getContentManager().registerOntology(ontology);
		
		//add this agent to the yellow pages
		DFAgentDescription dfd = new DFAgentDescription();
		dfd.setName(getAID());
		ServiceDescription sd = new ServiceDescription();
		sd.setType("supplier");
		sd.setName(getLocalName() + "-supplier-agent");
		dfd.addServices(sd);
		try{
			DFService.register(this, dfd);
		}
		catch(FIPAException e){
			e.printStackTrace();
		}
		
		addBehaviour(new TickerWaiter(this));
	}
	
	public class TickerWaiter extends CyclicBehaviour {

		//behaviour to wait for a new day
		public TickerWaiter(Agent a) {
			super(a);
		}

		@Override
		public void action() {
			MessageTemplate mt = MessageTemplate.or(MessageTemplate.MatchContent("new day"),
					MessageTemplate.MatchContent("terminate"));
			ACLMessage msg = myAgent.receive(mt); 
			if(msg != null) {
				if(tickerAgent == null) {
					tickerAgent = msg.getSender();
				}
				if(msg.getContent().equals("new day")) {

					ArrayList<Behaviour> cyclicBehaviours = new ArrayList<>();
					myAgent.addBehaviour(new EndDayListener(myAgent,cyclicBehaviours));
				}
				else {
					//termination message to end simulation
					myAgent.doDelete();
				}
			}
			else{
				block();
			}
		}
		
//		public class BookGenerator extends OneShotBehaviour {
//
//			@Override
//			public void action() {
//				booksForSale.clear();
//				//select one book for sale per day
//				int rand = (int)Math.round((1 + 2 * Math.random()));
//				//price will be between 1 and 50 GBP
//				int price = (int)Math.round((1 + 49 * Math.random()));
//				switch(rand) {
//					case 1 :
//						booksForSale.put("Java for Dummies", price);
//						break;
//					case 2 :
//						booksForSale.put("JADE: the Inside Story", price);
//						break;
//					case 3 :
//						booksForSale.put("Multi-Agent Systems for Everybody", price);
//						break;
//				}
//			}
//			
//		}
//		
//		public class FindBuyers extends OneShotBehaviour {
//
//			public FindBuyers(Agent a) {
//				super(a);
//			}
//
//			@Override
//			public void action() {
//				DFAgentDescription buyerTemplate = new DFAgentDescription();
//				ServiceDescription sd = new ServiceDescription();
//				sd.setType("buyer");
//				buyerTemplate.addServices(sd);
//				try{
//					buyers.clear();
//					DFAgentDescription[] agentsType1  = DFService.search(myAgent,buyerTemplate); 
//					for(int i=0; i<agentsType1.length; i++){
//						buyers.add(agentsType1[i].getName()); // this is the AID
//					}
//				}
//				catch(FIPAException e) {
//					e.printStackTrace();
//				}
//
//			}
//
//		}
//
//	}
//	
//	public class OffersServer extends CyclicBehaviour {
//		
//		public OffersServer(Agent a) {
//			super(a);
//		}
//
//		@Override
//		public void action() {
//			MessageTemplate mt = MessageTemplate.MatchPerformative(ACLMessage.CFP);
//			ACLMessage msg = myAgent.receive(mt);
//			if(msg != null) {
//				ACLMessage reply = msg.createReply();
//				String book = msg.getContent();
//				if(booksForSale.containsKey(book)) {
//					//we can send an offer
//					reply.setPerformative(ACLMessage.PROPOSE);
//					reply.setContent(String.valueOf(booksForSale.get(book)));
//				}
//				else {
//					reply.setPerformative(ACLMessage.REFUSE);
//				}
//				myAgent.send(reply);
//			}
//			else {
//				block();
//			}
//			
//		}
//		
//	}
//	
//	public class ResponseListener extends CyclicBehaviour {
//		
//		public ResponseListener(Agent a) {
//			super(a);
//		}
//
//		@Override
//		public void action() {
//			MessageTemplate mt = MessageTemplate.MatchPerformative(ACLMessage.ACCEPT_PROPOSAL);
//			ACLMessage msg = myAgent.receive(mt);
//			if(msg != null && msg.getSender() != tickerAgent) {
//				ACLMessage reply = msg.createReply();			
//				reply.setPerformative(ACLMessage.INFORM);
//				reply.setContent(String.valueOf("Purchased book!"));
//				myAgent.send(reply);
//			}
//			else {
//				block();
//			}
//			
//		}
//		
//	}
	
	public class EndDayListener extends CyclicBehaviour {
		private List <Behaviour> toRemove;
		
		public EndDayListener(Agent a, List<Behaviour> toRemove) {
			super(a);
			this.toRemove = toRemove;
		}

		@Override
		public void action() {
			MessageTemplate mt = MessageTemplate.MatchContent("done");
			ACLMessage msg = myAgent.receive(mt);
			
			if(msg != null){
				//we are finished
				ACLMessage tick = new ACLMessage(ACLMessage.INFORM);
				tick.setContent("done");
				tick.addReceiver(tickerAgent);
				myAgent.send(tick);
				//remove behaviours
				for(Behaviour behaviour :toRemove){
					myAgent.removeBehaviour(behaviour);
				}
				myAgent.removeBehaviour(this);
			}
			else{
				block();
			}
			
			

			}
		}
		
	}
}

