package coursework;
import jade.core.*;
import jade.core.Runtime;
import jade.wrapper.AgentController;
import jade.wrapper.ContainerController;


public class Main {

	public static void main(String[] args) {
		Profile myProfile = new ProfileImpl();
		Runtime myRuntime = Runtime.instance();
		try{
			ContainerController myContainer = myRuntime.createMainContainer(myProfile);	
			AgentController rma = myContainer.createNewAgent("rma", "jade.tools.rma.rma", null);
			rma.start();
			
			int numManufacturers = 1;
			int numSuppliers = 1;
			int numCustomers = 1;

			//Create specified number of Suppliers
			AgentController supplier;
			for(int i=0; i<numSuppliers; i++) {
				supplier = myContainer.createNewAgent("Supplier" + i, SupplierAgent.class.getCanonicalName(), null);
				supplier.start();
			}
			//Create specified number of Customers
			AgentController customer;
			for(int i=0; i<numCustomers; i++) {
				customer = myContainer.createNewAgent("Customer" + i, CustomerAgent.class.getCanonicalName(), null);
				customer.start();
			}
			//Create specified number of Manufacturers
			AgentController manufacturer;
			for(int i=0; i<numManufacturers; i++) {
				manufacturer = myContainer.createNewAgent("Manufacturer" + i, ManufacturerAgent.class.getCanonicalName(), null);
				manufacturer.start();
			}
			AgentController tickerAgent = myContainer.createNewAgent("DayTicker", DayTickerAgent.class.getCanonicalName(),
					null);
			tickerAgent.start();
			
		}
		catch(Exception e){
			System.out.println("Exception starting agent: " + e.toString());
		}


	}

}
